import pygame  #Imports Pygame
import sys  #Imports sys
from pygame.locals import *

pygame.init()


#The class below is the class that i have created for the launcher within the game it contains
#all of the variables and attributes that relate to the launcher.
class Launcher(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()
        self.image = pygame.image.load('LaserBase.png')  #loads the image for the launcher
        self.rect = self.image.get_rect()

        self.rocketXPos = 512  #sets the X position of the launcher

        self.rect.x = self.rocketXPos
        self.rect.y = 650  #sets the Y Position of the launcher


#The launcher update function is checked each frame
# depending on the key that is pressed the launcher movement is updated
    def update(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_RIGHT]:  #Checks to see of the right arrow key is pressed
            self.rocketXPos += 4  #If the right arrow key is pressed then the launcher moves 4 pixels to the right
        elif keys[pygame.K_LEFT]:  #Checks if the left arrow key is pressed
            self.rocketXPos -= 4  # Moves the launcher 4 pixels to the left
        if self.rocketXPos >= 900:
            self.rocketXPos = 900  #Sets the boundary of the launcher on the right side of the screen
        elif self.rocketXPos <= 100:
            self.rocketXPos = 100  #Sets the boundary of the launcher on the left side of the screen

        self.rect.x = self.rocketXPos
        self.rect.y = 650


#Below shows the class for the invaders, it contains all the attributes and variable relating to the invaders.
class Invader(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()
        self.image = pygame.image.load('inv12.png')  #Loads the inv12.png image as the image to use for the invaders
        self.rect = self.image.get_rect()
        self.__alienPosX = 0  #Sets the x position of the invaders to zero
        self.__alienPosY = 0  #Sets the y position of the invaders to zero
        self.__listPos = 0  # Sets the list position of the invaders to zero
        self.alienSpeed = 6  #The speed that the invaders move
        self.alienDirection = -1  #Sets the movement direction of the invaders when the game first starts up


    def setPosX(self, x):  #Sets the x position of the invaders
        self.__alienPosX = x  #Sets the x position of the invader equal to x
        self.rect.x = x

    def setPosY(self, y):  #ses the y position of the invaders
        self.__alienPosY = y  #Sets the y position of the alien equal to y
        self.rect.y = y

    def setListPos(self, pos):  #Sets the position of the invaders within the list
        self.__listPos = pos

    def update(self):  #Invaedr update function check the position of the invaders each frame
        self.__alienPosX += self.alienSpeed * self.alienDirection

        if self.__alienPosX < 100 + (self.__listPos * 32):
            self.__alienPosY += 15  #Distace invaders move down the screen
            self.alienDirection = +1  #Moves the invaders to the right
        elif self.__alienPosX > 924 - ((10 - self.__listPos) * 32):
            self.__alienPosY += 15  #Distace invaders move down the screen
            self.alienDirection = -1  #Moves the invaders to the left

        self.rect.x = self.__alienPosX  #Sets self.rect.x variable equal to the self.__alienPosX variable
        self.rect.y = self.__alienPosY  #Sets self.rect.y variable equal to the self.__alienPosY variable


#Below is the missile class containing all of the attributes and variables for the missile
class Missile(pygame.sprite.Sprite):
    def __init__(self, xInitialPos, yInitialPos):
        super().__init__()
        self.image = pygame.image.load('bullet.png')  #Loads the the bullet.png image as the image for the missile
        self.rect = self.image.get_rect()

        self.__missilePosY = yInitialPos  #Sets the y position of the missile

        self.rect.x = xInitialPos  #Sets self.rect.x variable equal to the initial x position
        self.rect.y = self.__missilePosY

    def update(self):  #Checks the y position of the missile
        self.__missilePosY -= 4  #sets the speed at which the missile moves when it is fired
        self.rect.y = self.__missilePosY

#Below is the class for the game it contains all of the variables for the variables that relate to the game
class Game:
    def __init__(self):
        pygame.init()
        pygame.key.set_repeat(1, 1)

        self.width = 1024  #sets the width of the game screen
        self.height = 768  #sets the height of the game screen
        self.screen = pygame.display.set_mode((self.width, self.height))
        self.caption = "Space Invaders!!"  #sets the lable at the top of the window that the game is in
        pygame.display.set_caption(self.caption)

        self.framerate = 60  #sets the framerate of the game

        self.clock = pygame.time.Clock()  #keeps track of the plying time of the game
        self.starfieldImg = pygame.image.load('Starfield1024x768.png')  #loads the game background image

        self.font = pygame.font.Font(None, 40)  #sets the font style amd size that is used on the start and end screen of the game

        self.allsprites = pygame.sprite.Group()  #creats a sprite group called self.allsprites
        self.missiles = pygame.sprite.Group()  #creats a sprite group called self.missiles
        self.launcher = Launcher()  #Sets self.launcher equal to Launcher class
        self.invader = Invader()  #Sets self.invader equal to Invader class
        self.aliens = pygame.sprite.Group()  #creats a sprite group called self.aliens
        self.allsprites.add(self.launcher, self.aliens, self.missiles)  #adds the following elements to the self.allsprites group
        self.missileFired = None  #Sets the variable self.missileFired equal to zero
        self.explosionSound = pygame.mixer.Sound("explosion.wav")  #Loads and saves the explosion sound in memory
        self.laserSound = pygame.mixer.Sound("laser.wav")  #Loads and saves the laser sound in memory
        self.backgroundMusic = pygame.mixer.music.load("background.mp3") #Loads and saves the background music in memory

        #This for loop duplicates the loop that creates the first line of the invaders.
        #This creates 5 rows of 11 invaders
        ypos = 100
        for J in range(5):

            xPos = 512

            for i in range(11):
                invader = Invader()
                invader.setPosX(xPos)
                invader.setPosY(ypos)
                invader.setListPos(i)
                self.allsprites.add(invader)
                self.aliens.add(invader)
                xPos += 32
            ypos+= 32

        self.playerscore = 0  #set the self.playerscore variable to zero

#This is the main game loop for my game it ensures that all the functions are called upon in the order they are needed
    def run(self):
        self.menu = True  #Sets self.menu variable equal to True
        while self.menu == True:
            self.updatestarted()  #Calls updatestarted function
            self.drawstarted()  #Calls drawstarted function
            self.clock.tick(self.framerate)

        while True:
            if len(self.aliens) != 0:  #checks if the length of the self.aliens group does not equal zero
                self.drawgame()  #Calls the draw game function
                self.updategame()  #Calls the updategame function
                self.clock.tick(self.framerate)

            if len(self.aliens) == 0:  #checks if the length of the self.aliens group is equal to zero
                self.drawend()  #Calls the drawend function
                self.updatend()  #Calls the updatend function

    def updatestarted(self):  #The function below is the update function for the start menu of the game
        events = pygame.event.get()

        for event in events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_s:  #Checks if the 's' key is pressed
                    self.menu = False  #Sets self.menu variable equal to False
                    pygame.mixer.music.play(2)  #plays background music when the game starts
                    pygame.mixer.music.set_volume(0.1)  #sets the volume of the background music
                    break

            if event.type == pygame.QUIT:  #checks to see if the exit button has been pressed
                    pygame.quit()  #if so the game window is closed.
                    sys.exit()

    def updategame(self):
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:  #checks to see if the exit button has been pressed if so the game window is closed.
                    pygame.quit()
                    sys.exit()
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_SPACE:  #checks to see if space bar has been pressed
                    self.missileFired = Missile(self.launcher.rocketXPos, 650)
                    self.missiles.add(self.missileFired)  #adds missileFired to self.missiles group
                    self.allsprites.add(self.missileFired)  #Adds missileFired to the self.allsprites group
                    self.laserSound.play()  #Plays the laser.wav sound effect

        if pygame.sprite.spritecollideany(self.launcher, self.aliens):  #checks collision between launcher and invaders
            self.aliens.empty()  #Removes all aliens to bring up ending screen when launcher and invaders collide

        self.allsprites.update()  #Updates the self.allsprites group

#The if statement below checks the collision between the invaders and the missile
        if len(pygame.sprite.groupcollide(self.missiles, self.aliens, True, True)) != 0:
            self.playerscore += 100  #adds 100 to the player score if collision is true
            self.explosionSound.play()  #plays explosion sound if collision is true


#The Function below is the update function is the update function for the end screen of the game
    def updatend(self):
        events = pygame.event.get()

        for event in events:
            if event.type == pygame.KEYDOWN:  #Checks if key is pressed down
                if event.key == pygame.K_r:  #If the r key is pressed then the game is restarted.
                    Game().run()  #Restarts the game
                    break

            if event.key == pygame.K_x:  #checks to see if the 'x' key is pressed if so the game is closed
                    pygame.quit()
                    sys.exit()

#The function below if the draw method for the start menu of the game
    def drawstarted(self):
        self.screen.blit(self.starfieldImg, (0, 0))  #loads the background image for the start screen
        width, height = self.font.size("S P A C E  I N V A D E R S!")
        text = self.font.render("S P A C E  I N V A D E R S!", True, (0, 255, 0))  #Renders text onto the game screen
        xPos = (1024 - width)/2  #Sets the x position of the rendered text
        self.screen.blit(text, (xPos, 200))  #Sets the x position of the text on the screen
        width, height = self.font.size("P R E S S  'S'  T O  S T A R T")
        text = self.font.render("P R E S S  'S'  T O  S T A R T", True, (0, 255, 0))  #Renders text onto the game screen
        xPos = (1024 - width)/2  #Sets the x position of the rendered text
        self.screen.blit(text, (xPos, 400))  #sets the x position of the text on the screen
        pygame.display.flip()  #Updates the game start screen

#The function below is the draw method for the game itself
    def drawgame(self):
        self.screen.blit(self.starfieldImg, (0, 0))  #loads the background image for the start screen
        self.allsprites.draw(self.screen)  #Draws/Displays the self.allsprites group to the game screen
        #The line below renders the players score onto the screen
        self.score_text = self.font.render("Score : %d" %self.playerscore, True, (0, 255, 0))
        self.screen.blit(self.score_text, (10, 10)) #Sets the position of the rendered player score
        pygame.display.flip()  #Updates the game screen

#The function below is the draw method for the ending screen of the game
    def drawend(self):
        self.screen.blit(self.starfieldImg, (0, 0))  #Sets the background of the end game screen
        width, height = self.font.size("P R E S S  'R'  T O  R E S T A R T  G A M E")
        text = self.font.render("P R E S S  'R'  T O  R E S T A R T  G A M E", True, (0, 255, 0))  #Renders text onto the game screen
        xPos = (1024 - width)/2  #Sets the x position of the rendered text
        self.screen.blit(text, (xPos, 200))  #Sets the x position of the text on the screen
        width, height = self.font.size("P R E S S  'X'  T O  E X I T   G A M E")
        text = self.font.render("P R E S S  'X'  T O  E X I T  G A M E", True, (0, 255, 0))  #Renders text onto the game screen
        xPos = (1024 - width)/2  #Sets the x position of the rendered text
        self.screen.blit(text, (xPos, 300))  #Sets the x position of the text on the screen
        width, height = self.font.size("F I N A L  S C O R E  :  %d" %self.playerscore)
        text = self.font.render("F I N A L  S C O R E  :  %d" %self.playerscore, True, (0, 255, 0))  #displays the final game score on the screen
        xPos = (1024 - width)/2  #Sets the x position of the rendered text
        self.screen.blit(text, (xPos, 400))  #Sets the x position of the text on the screen
        pygame.display.flip()  #Updates the game end screen

if __name__ == '__main__':
    game = Game().run()  #Runs the game


